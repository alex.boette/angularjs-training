'use strict';

// Register `phoneDetail` component, along with its associated controller and template
angular.module('phoneDetail').
  controller('PhoneDetailController', ['$scope', '$routeParams', 'Phone',
    function ($scope, $routeParams, Phone) {
      $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
        $scope.setImage(phone.images[0]);
      });

      $scope.setImage = function setImage(imageUrl) {
        $scope.img = imageUrl;
      };
    }
  ]
);
