'use strict';

angular.
  module('phonecatApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/phones', {
          templateUrl: '/phone-list/phone-list.template.html',
          controller: 'PhoneListController'
        }).
        when('/phones/:phoneId', {
          templateUrl: '/phone-detail/phone-detail.template.html',
          controller: 'PhoneDetailController'
        }).
        when('/sales', {
          templateUrl: '/sales-statistics/sales-statistics.template.html',
          controller: 'SalesStatisticsController'
        }).
        otherwise('/phones');
    }
  ]);
