'use strict';

// Define the `phonecatApp` module
angular.module('phonecatApp', [
  'ngRoute',
  'core',
  'ui.grid',
  'ui.bootstrap',
  'phoneDetail',
  'phoneList',
  'charts',
  'salesStatistics'
]);
