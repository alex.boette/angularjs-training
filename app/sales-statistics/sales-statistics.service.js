'use strict';

angular
  .module('salesStatistics')
  .factory('SalesStatisticsService', ['Phone',
    function (Phone) {
      return Phone.query().$promise.then(function(phones) {
        return phones.map(phone => {
          return {
            name: phone.name,
            data: Object.values(phone.sales)
            }          
          }
        );
      });
    }
  ]
);