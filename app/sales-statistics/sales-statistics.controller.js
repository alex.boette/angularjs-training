'use strict';

angular.module('salesStatistics').
  controller('SalesStatisticsController', ['$scope', 'SalesStatisticsService',
    function ($scope, SalesStatisticsService) {
      SalesStatisticsService.then( (data) => {
        $scope.phones = data;
      });
    }
  ]
);