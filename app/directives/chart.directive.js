angular
  .module("charts")
  .directive('chart', function() {
    return {
      replace: true,
      scope: {        
        data: '=',
        title: '@',
        yAxis: '@',
        point: '@'
      },
      template:'<div id="container"></div>',
      link: function(scope){
        Highcharts.chart('container', {
          title: { text: scope.title },
          yAxis: { title: { text: scope.yAxis } },
          legend: { layout: 'vertical', align: 'right', verticalAlign: 'middle' },
          plotOptions: { series: {
            label: { connectorAllowed: false },
            pointStart: Number(scope.point) } 
          },
          series: scope.data,
          responsive: { rules: [{
            condition: { maxWidth: 500 },
            chartOptions: { legend: { layout: 'horizontal', align: 'center', verticalAlign: 'bottom' } } 
          }]}
        });
      }
    };
  }
);
