'use strict';

// Register `phoneList` component, along with its associated controller and template

angular.module('phoneList')
  .controller('PhoneListController', ['$scope', '$filter', 'uiGridConstants', 'Phone',
    function ($scope, $filter, uiGridConstants, Phone) {
        $scope.orderProp = 'age';
        let phones = Phone.query();
        let imageTemplate = '<img width="50px" ng-src="{{grid.getCellValue(row, col)}}"/>';
        let linkTemplate =  '<a href="#!/phones/{{grid.getCellValue(row, col)}}">'+
                              '<button type="button" class="btn btn-info">Show More</button>'+
                            '</a>';

        $scope.gridOptions = {
          data: phones,
          enableFiltering: false,
          rowHeight:80,
          onRegisterApi: function(gridApi){
            $scope.gridApi = gridApi;
            $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
          },
          columnDefs: [
            { field: 'imageUrl', displayName: 'Image', cellTemplate: imageTemplate, width:'5%'},
            { field: 'name', width:'25%' },
            { field: 'snippet', width:'62%' },
            { field: 'id', displayName: '', cellTemplate: linkTemplate, width:'8%' }
          ]
        };

        $scope.filter = function() {
          $scope.gridApi.grid.refresh();
        };

        $scope.singleFilter = function( renderableRows ){
          var matcher = new RegExp($scope.filterValue);
          renderableRows.forEach( function( row ) {
            var match = false;
            [ 'name' ].forEach(function( field ){
              if ( row.entity[field].match(matcher) ){
                match = true;
              }
            });
            if ( !match ){
              row.visible = false;
            }
          });
          return renderableRows;
        };

        $scope.sort = function() {
          $scope.gridOptions.data = $filter('orderBy')($scope.gridOptions.data, $scope.orderProp, false)
          $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
        };

      }
    ]);